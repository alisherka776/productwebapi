﻿using Microsoft.AspNetCore.Mvc;
using ProductWebApi.Models;
using ProductWebApi.Services.CategoryRepository;

namespace ProductWebApi.Controllers
{
    [ApiController]
    [Route("api/category")]
    public class CategoryController : ControllerBase
    {
        private readonly ICategoryRepo _repository;

        public CategoryController(ICategoryRepo repository)
        {
            _repository = repository;
        }

        [HttpGet]

        public async Task<IActionResult> GetCategories()
        {
            try
            {
                var categories = await _repository.GetCategoriesAsync();
                return Ok(categories);
            }
            catch (Exception)
            {
                throw;
            }
        }

        [HttpPost]
        public async Task<IActionResult> CreateCategory([FromBody] Category category)
        {
            try
            {
                await _repository.CreateCategoryAsync(category);
                return Ok(category);
            }
            catch (Exception)
            {
                throw;
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCategory(int id)
        {
            try
            {
                await _repository.DeleteCategoryAsync(id);
                return NoContent();
            }
            catch (Exception)
            {

                throw;
            }
        }

    }

}
