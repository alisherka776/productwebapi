﻿using Microsoft.AspNetCore.Mvc;
using ProductWebApi.Models;
using ProductWebApi.Services.ProductRepository;

namespace ProductWebApi.Controllers
{
    [Route("api/product")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly IProductRepo _repository;
        public ProductController(IProductRepo repository)
        {
            _repository = repository;
        }

        [HttpGet]
        public async Task<IActionResult> GetProducts()
        {
            try
            {
                var products = await _repository.GetProductsAsync();
                return Ok(products);
            }
            catch (Exception)
            {
                throw;
            }
        }

        [HttpPost]
        public async Task<IActionResult> CreateProduct([FromBody] Product product)
        {
            try
            {
                await _repository.CreateProductAsync(product);
                return Ok(product);
            }
            catch (Exception)
            {
                throw;
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteProduct(int id)
        {
            try
            {
                await _repository.DeleteProductAsync(id);
                return NoContent();
            }
            catch (Exception)
            {

                throw;
            }
        }

    }
}
