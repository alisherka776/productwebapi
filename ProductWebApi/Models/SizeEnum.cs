﻿namespace ProductWebApi.Models
{
    public enum SizeEnum
    {
        Big,
        Medium,
        Small
    }
}
