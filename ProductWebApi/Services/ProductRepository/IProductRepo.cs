﻿using ProductWebApi.Models;

namespace ProductWebApi.Services.ProductRepository
{
    public interface IProductRepo
    {
        Task<IEnumerable<Product>> GetProductsAsync();
        Task CreateProductAsync (Product product);
        Task DeleteProductAsync (int id);
        Task<Product> GetProductByIdAsync(int id);
    }
}
