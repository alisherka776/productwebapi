﻿using Microsoft.EntityFrameworkCore;
using ProductWebApi.Data;
using ProductWebApi.Models;

namespace ProductWebApi.Services.ProductRepository
{
    public class ProductRepo : IProductRepo
    {
        private readonly ProductWebApiDbContext _context;
        public ProductRepo(ProductWebApiDbContext context)
        {
            _context = context;
        }
        public async Task CreateProductAsync(Product product)
        {
            await _context.Products.AddAsync(product);
            await _context.SaveChangesAsync();
        }

        public async Task DeleteProductAsync(int id)
        {
            var product = await GetProductByIdAsync(id);

            _context.Products.Remove(product);
            await _context.SaveChangesAsync();

        }

        public async Task<Product> GetProductByIdAsync(int id)
        {
            return await _context.Products.FindAsync(id) ?? throw new NullReferenceException($"{id} not found");
        }

        public async Task<IEnumerable<Product>> GetProductsAsync()
        {
            var query = _context.Products.AsQueryable();
            return await query
                .OrderBy(q => q.CategoryId).ToListAsync();
        }
    }
}
