﻿using ProductWebApi.Models;

namespace ProductWebApi.Services.CategoryRepository
{
    public interface ICategoryRepo
    {
        Task<IEnumerable<Category>> GetCategoriesAsync();
        Task CreateCategoryAsync (Category category);
        Task DeleteCategoryAsync (int id);
        Task<Category> GetCategoryByIdAsync (int id);   
    }
}
