﻿using Microsoft.EntityFrameworkCore;
using ProductWebApi.Models;

namespace ProductWebApi.Data
{
    public class ProductWebApiDbContext : DbContext
    {
        public DbSet<Product> Products { get; set; } 
        public DbSet<Category> Categories { get; set; }
        public ProductWebApiDbContext(DbContextOptions<ProductWebApiDbContext> options) : base(options)
        {

        }

        public ProductWebApiDbContext() : base()
        {
            
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Product>(entity =>
            {
                entity.HasKey(p => p.Id);
                entity.Property(p => p.Name).IsRequired();
                entity.Property(p => p.Description).IsRequired();
                entity.Property(p => p.Price).IsRequired();
                entity.HasOne(p => p.Category).WithMany(p => p.Product).HasForeignKey(p => p.CategoryId);
            });
        }
    }
}
